#include <stdbool.h>
#include <stdio.h>

bool checkArgs(int count){
      if(count != 4){
        fprintf(stderr, "Incorrect number of arguments. Three arguments: <image> <rotate-image> <angle>\n");
        return false;
      }
      return true;
}

bool checkAngle(__int64_t* angle){
      if(*angle < 0)
        *angle = 360 + *angle;
      if(*angle % 90 != 0){
        fprintf(stderr, "Wrong angle\n");
        return false;
      }
      return true;
}

bool checkOpenFile(FILE *input){    
    if (input == NULL) {
        fprintf(stderr, "Cannot find input file\n");
        return false;
    }
    return true;
}
