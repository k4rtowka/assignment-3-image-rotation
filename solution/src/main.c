#include "utils.h"
#include "bmp.h"
#include "rotate.h"

#include <stdlib.h>


int main( int argc, char** argv ) {
    
    //check all params
    if(!checkArgs(argc)) return 1;

    __int64_t angle = atoi(argv[3]);
    if(!checkAngle(&angle)) return 2;

    //open image
    FILE *input = fopen(argv[1], "rb");
    if(!checkOpenFile(input)) return 3;
    


    struct image image;
    enum read_status read_bmp = from_bmp(input, &image);

    if (read_bmp != READ_OK) {
        fprintf(stderr, "Error during reading bmp\n");
        free_image(&image);
        fclose(input);
        return 4;
    }
    fclose(input);
    struct image rotated;
    rotated = rotate(image, angle);
    free_image(&image);

        FILE *output = fopen(argv[2], "wb");

        if (output == NULL)
        {
            fprintf(stderr, "Cannot find output file\n");
            free_image(&rotated);
            return 5;
        }

    enum write_status write_bmp = to_bmp(output, &rotated);

    if (write_bmp != WRITE_OK)
    {
        fprintf(stderr, "Error during writing\n");
        free_image(&rotated);
        fclose(output);
        return 6;
    }

    free_image(&rotated);
    fclose(output);
    return 0;
}
