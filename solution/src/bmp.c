#include "bmp.h"
#include <stdint.h>
#include <stdio.h>

    const uint16_t BFtype = 19778;
    const uint16_t BFBITCount = 24;
    const uint32_t BISize = 40;
    const uint16_t BIPlanes = 1;

static uint32_t get_padding(uint32_t width)
{
    const uint32_t byteAligment = 4;
    uint32_t row_size = width * sizeof(struct pixel);
    return (byteAligment - row_size % byteAligment) % byteAligment;
}

struct bmp_header create_header(struct image const *img){
    struct bmp_header header;


    header.bfType = BFtype;
    header.bfileSize = sizeof(struct bmp_header) + img->height * (img->width * sizeof(struct pixel) + get_padding(img->width));
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BISize;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BIPlanes;
    header.biBitCount = BFBITCount;
    header.biCompression = 0;
    header.biSizeImage = img->height * (img->width * sizeof(struct pixel) + get_padding(img->width));
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}

enum read_status from_bmp(FILE *input, struct image *img) {
    struct bmp_header bmp = {0};
    if (!fread(&bmp, sizeof(struct bmp_header), 1, input)) {
        return READ_INVALID_HEADER;
    }
    if(bmp.biBitCount != BFBITCount && bmp.bfType != BFtype){
        return READ_INVALID_HEADER;
    }

    *img = create_image(bmp.biWidth, bmp.biHeight);

    uint32_t padding = get_padding(img->width);
    for (uint32_t i = 0; i < bmp.biHeight; i++)
    {
        if (fread(img->data + i * bmp.biWidth, sizeof(struct pixel), bmp.biWidth, input) != bmp.biWidth)
        {
            return READ_INVALID_BITS;
        }
        fseek(input, (long)padding, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img)
{
    struct bmp_header header = create_header(img);

    size_t count = fwrite(&header, sizeof(struct bmp_header), 1, out);

    if (count != 1)
    {
        return WRITE_INVALID_HEADER;
    }

    for (uint32_t i = 0; i < img->height; i++)
    {
        const void *offset = img->data + i * (img->width);
        if (!fwrite(offset, 3, img->width, out))
        {
            return WRITE_ERROR;
        }
        if(fseek( out ,  get_padding(img->width), SEEK_CUR ))
        {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}


