#include "image.h"
#include <stdint.h>

#define CREATE_ROTATE(side, RotatePixels, Pixels, isSpin) \
    struct image rotate##side(struct image const source){ \
            struct image rotated_img = {0}; \
            if((isSpin) == 1){ \
                 rotated_img = create_image(source.height, source.width); \
            }else{ \
                rotated_img = create_image(source.width, source.height); \
            } \
            for (uint32_t row = 0; row < source.height; row++) { \
                for (uint32_t col = 0; col < source.width; col++) { \
                    rotated_img.data[(RotatePixels)] = source.data[(Pixels)]; \
                } \
            } \
    return rotated_img; \
    } 

CREATE_ROTATE(Right, (source.width - col - 1) * source.height + row, source.width * row + col, 1)
CREATE_ROTATE(Left, source.height * (col + 1) - row - 1, source.width * row + col, 1)
CREATE_ROTATE(180, source.width * row + col, source.width * (source.height - row - 1) + source.width - col - 1, 0)
CREATE_ROTATE(Default, source.width * row + col, source.width * row + col, 0)
//source_row = source.height - row - 1;
//source_col = source.width - col - 1;
//rotated_img.data(source.width - col - 1) - новый индекс столбца в повернутом изображении.
//rotated_img.data(source.width - row - 1) - новый индекс строки в повернутом изображении.
//source.data[source.width * row + col] - значение пикселя из исходного изображения с использованием индексов строки и столбца в исходном изображении

struct image rotate(struct image const source, int64_t angle){
    if(angle == 270){
        return rotateLeft(source);
    }
    if(angle == 180){
        return rotate180(source);
    }
    if(angle == 90){
        return rotateRight(source);
    }else
        return rotateDefault(source);
}
