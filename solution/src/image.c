#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct image create_image(const uint32_t width, const uint32_t height) {
    void* data = malloc(sizeof(struct pixel) * width * height);
    if(data == NULL){
        fprintf(stderr, "Не удалось выделить память");
    }
    return (struct image) {
            .width = width,
            .height = height,
            .data = data
    };
}

void free_image(struct image *image) {
    if (image->data == NULL) {
        return;
    }
    free(image->data);
}
