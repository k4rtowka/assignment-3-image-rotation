#include <stdbool.h>
#include <stdio.h>

bool checkArgs(int count);

bool checkAngle(__int64_t* angle);

bool checkOpenFile(FILE *input);
